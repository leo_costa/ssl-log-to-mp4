#!/usr/bin/python3.10
from LogConverter import LogConverter

from aux.RobotBall import BLUE_TEAM
from aux.utils import red_print, blue_print


if __name__ == "__main__":
    converter = LogConverter(False)
    log_frames, _ = converter.log_parser.read_log_file()

    # Se mudar para True ele vai mostrar TODOS os dados enquanto lê o arquivo
    show_log_data = False
    for i in range(0, log_frames):
        detection, geometry, referee = converter.get_log_data(i, show_log_data)

        # Exemplo mostrando a posição de todos os robôs do time Azul
        for robot in detection['bots']:
            if robot['id']['color'] == BLUE_TEAM:
                blue_print("Robô [{}] -> Pos {}".format(robot['id']['number'],
                                                        robot['obj']['pos']))

    red_print("Quit")
