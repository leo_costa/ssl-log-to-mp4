#!/usr/bin/python3.10
import argparse
import datetime
from pathlib import Path

from os.path import join
from os import makedirs, getcwd
import moviepy.editor as mpy

from LogParser import LogParser, LogData
from DrawSSL import DrawSSL

from aux.RobotBall import Robot, Ball, BLUE_TEAM, YELLOW_TEAM
from aux.utils import red_print, blue_print, green_print, purple_print

MAX_ROBOTS = 16
VIDEO_FPS = 60

# Example
# RoboCup 2021 Online @ Small Size League - Team A vs Team B - Part X
YEAR = datetime.datetime.utcnow().year
FILENAME_FORMAT = [f'RoboCup {YEAR} Online @ Small Size League - ',
                   ' vs ', ' - Part ']


class LogConverter():
    def __init__(self, generate_video=True):
        args = self.parse_args()
        global VIDEO_FPS
        VIDEO_FPS = int(args['video_fps'])

        if generate_video:
            # Prepare Video Folder
            self.video_folder = join(getcwd(), 'Video')
            makedirs(self.video_folder, exist_ok=True)
            green_print(f'Saving video to {self.video_folder}')

        # Get log filename
        self.log_filename = str(args['log_file'])
        blue_print(f'[LOG INFO] Openning {self.log_filename}')
        self.log_parser = LogParser(self.log_filename, VIDEO_FPS)

        # Init data
        self.last_referee_cmd = 'STOP'
        self.last_timestamp = 0
        self.team_name = {YELLOW_TEAM: 'Unknown', BLUE_TEAM: 'Unknown'}
        self.ball = Ball()
        self.blue_robots = [Robot(team=BLUE_TEAM, robot_id=i)
                            for i in range(MAX_ROBOTS)]
        self.yellow_robots = [Robot(team=YELLOW_TEAM, robot_id=i)
                              for i in range(MAX_ROBOTS)]

        # Init field renderer
        self.draw = DrawSSL()

# =============================================================================

    def parse_args(self):
        arg_parser = argparse.ArgumentParser()
        arg_parser.add_argument('-f', '--log-file', required=True,
                                help='Log file to be transformed into a mp4')
        arg_parser.add_argument('-v', '--video-fps', required=False,
                                help=f'FPS of the field. NOTE: this will \
                                influence the video speed. Default is \
                                {VIDEO_FPS} FPS',
                                default=VIDEO_FPS)

        return vars(arg_parser.parse_args())

# =============================================================================

    def update_vision_data(self, vision_data, geometry_data):

        if geometry_data is not None:
            if 'field_size' in geometry_data.keys():
                self.draw.set_field_size(geometry_data['field_size'])

            if 'goal_size' in geometry_data.keys():
                self.draw.set_goal_size(geometry_data['goal_size'])

            if 'center_circle' in geometry_data.keys():
                self.draw.set_center_circle_radius(
                    geometry_data['center_circle'])

            if 'right_penalty' in geometry_data.keys():
                self.draw.set_penalty_size(geometry_data['right_penalty'],
                                           'right')
            if 'left_penalty' in geometry_data.keys():
                self.draw.set_penalty_size(geometry_data['left_penalty'],
                                           'left')

        if vision_data is not None and len(vision_data) > 0:
            data_keys = list(vision_data.keys())
            if 'ball' in data_keys:
                self.ball.update(pos=vision_data['ball'])
                self.draw.update_ball(self.ball.pos)

            blue_bots = []
            yellow_bots = []
            if 'bots' in data_keys:
                for robot in vision_data['bots']:
                    for r_id in range(MAX_ROBOTS):
                        self.blue_robots[r_id].update(obj=robot['obj'],
                                                      id=robot['id'])

                        self.yellow_robots[r_id].update(obj=robot['obj'],
                                                        id=robot['id'])
                # In case there are no robots in the field, make sure they get
                # their 'in_vision' state updated
                if len(vision_data['bots']) == 0:
                    for r_id in range(MAX_ROBOTS):
                        self.blue_robots[r_id].update(
                            id={'number': r_id, 'color': BLUE_TEAM})
                        self.yellow_robots[r_id].update(
                            id={'number': r_id, 'color': YELLOW_TEAM})

                blue_bots.extend([bot for bot in self.blue_robots
                                  if bot.in_vision()])
                yellow_bots.extend([bot for bot in self.yellow_robots
                                    if bot.in_vision()])

            self.draw.update_robots(blue_bots, BLUE_TEAM)
            self.draw.update_robots(yellow_bots, YELLOW_TEAM)

# =============================================================================

    def update_referee_data(self, referee_data):
        if len(referee_data) > 0:
            self.team_name = referee_data['TeamNames']
            self.draw.set_referee_data(referee_data['Command'],
                                       referee_data['Stage'],
                                       referee_data['StageTime'],
                                       referee_data['TeamNames'],
                                       referee_data['TeamScores'],
                                       referee_data['TeamYellowC'],
                                       referee_data['TeamRedC'])

# =============================================================================

    def get_filtered_log_data(self) -> LogData:
        log_data = self.log_parser.get_log_data()

        if len(log_data.referee) > 0:
            self.last_referee_cmd = log_data.referee['Command']

        while self.last_referee_cmd in ['STOP', 'HALT'] and not log_data.end:
            log_data = self.log_parser.get_log_data()
            if len(log_data.referee) > 0:
                self.last_referee_cmd = log_data.referee['Command']

        self.last_timestamp = log_data.timestamp
        return log_data
# =============================================================================

    def get_log_data(self, i, show_data=False):
        log_data = self.log_parser.get_data_by_frame(i)

        if show_data:
            green_print("[LOG DATA] Detection at frame = {}\n{}".format(
                i, log_data.detection))
            blue_print("[LOG DATA] Geometry at frame = {}\n{}".format(
                i, log_data.geometry))
            purple_print("[LOG DATA] Referee at frame = {}\n{}".format(
                i, log_data.referee))

        return log_data.detection, log_data.geometry, log_data.referee

# =============================================================================

    def create_video(self, t):
        #          log_data = self.get_filtered_log_data()
        log_data = self.log_parser.get_video_frame_data(t)

        self.update_vision_data(log_data.detection, log_data.geometry)
        self.update_referee_data(log_data.referee)

        return self.draw.render_field()

    def generate_filename(self) -> str:
        filename = FILENAME_FORMAT
        filename.insert(1, self.team_name[YELLOW_TEAM])
        filename.insert(3, self.team_name[BLUE_TEAM])
        i = 0
        file_path = None
        backup_filename = filename.copy()

        while True:
            filename = backup_filename.copy()
            filename.append(f'{i}.mp4')
            file_path = Path(join(self.video_folder, ''.join(filename)))

            if file_path.exists():
                i += 1
            else:
                break
        return str(file_path.resolve())

# =============================================================================


if __name__ == "__main__":
    converter = LogConverter()
    frames, log_duration = converter.log_parser.read_log_file()
    blue_print(
        f'[LOG CONVERTER] Log file has {log_duration} seconds of gameplay')
    blue_print(
        f'[LOG CONVERTER] Generating video with {VIDEO_FPS} FPS')

    field_clip = mpy.VideoClip(converter.create_video, duration=log_duration)

    video_name = converter.generate_filename()
    field_clip.write_videofile(video_name, fps=VIDEO_FPS)

    green_print(f'\n[LOG CONVERTER] Saving {video_name}')

    red_print('\nQuit')
