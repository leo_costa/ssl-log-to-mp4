import gizeh as gz
import numpy as np
from math import sqrt, acos, pi, trunc, cos, sin

from aux.RobotBall import Robot, Position, BLUE_TEAM, YELLOW_TEAM, BALL, INF
from aux.utils import red_print, blue_print, green_print, purple_print

FIELD_LINE_PEN_SZ = 6
# SCREEN_SIZE = [1024, 768]
SCREEN_SIZE = [1920, 1080]
FIELD_BORDER = 500
CARD_SZ = [150, 200]

BALL_RADIUS = 21.5
BOT_RADIUS = 90

BLUE_C = (0, 204/255, 255/255)  # = #00ccff
C_BLUE_C = (102/255, 204/255, 255/255)  # = #66ccff

YELLOW_C = (255/255, 255/255, 0)  # = #ffff00
D_YELLOW_C = (153/255, 102/255, 0)  # = #996600

RED_C = (255/255, 0, 0)

GREEN_C = (153/255, 255/255, 153/255)  # = #99ff99
D_GREEN_C = (0, 102/255, 0)  # = #006600

BLACK_C = (0, 0, 0)
WHITE_C = (1, 1, 1)

ORANGE_C = (255/255, 153/255, 0/255)  # = #ff9900


class RefereeData(object):
    def __init__(self):
        self.cmd = ''
        self.stage = ''
        self.stage_time = ''
        self.goals = {YELLOW_TEAM: 0, BLUE_TEAM: 0}
        self.yellow_cards = {YELLOW_TEAM: 0, BLUE_TEAM: 0}
        self.red_cards = {YELLOW_TEAM: 0, BLUE_TEAM: 0}
        self.names = {YELLOW_TEAM: 'Unknown', BLUE_TEAM: 'Unknown'}


class DrawSSL(object):
    def __init__(self):
        self.field_size = np.array(SCREEN_SIZE)
        self.goal_size = np.array([0, 0])
        self.center_circle_radius = 100
        self.right_penalty = [[0, 0], [0, 0], [0, 0], [0, 0]]
        self.left_penalty = [[0, 0], [0, 0], [0, 0], [0, 0]]

        self.ball = np.array([INF, INF])

        self.blue_robots = []
        self.yellow_robots = []
        self.referee_data = RefereeData()

# =============================================================================

    def render_field(self):
        self.update_drawings()
        return self.surface.get_npimage()

# =============================================================================

    def update_drawings(self):
        self.surface = gz.Surface(SCREEN_SIZE[0], SCREEN_SIZE[1],
                                  bg_color=D_GREEN_C)

        scaled_field = self.scale(self.field_size)
        self.draw_goal(scaled_field, 'left')
        self.draw_goal(scaled_field, 'right')
        self.draw_field(scaled_field)
        self.draw_penalty(self.right_penalty, scaled_field)
        self.draw_penalty(self.left_penalty, scaled_field)

        self.draw_robots(scaled_field)
        self.draw_ball(scaled_field)
        self.show_referee_data(scaled_field)

# =============================================================================

    def set_field_size(self, new_size: [int, int]):
        self.field_size = np.array(new_size)

# =============================================================================

    def set_goal_size(self, new_size: [int, int]):
        self.goal_size = np.array(new_size)

# =============================================================================

    def set_center_circle_radius(self, radius: int):
        self.center_circle_radius = radius

# =============================================================================

    def set_penalty_size(self, penalty: [], side: str):
        if side == 'right':
            self.right_penalty = penalty
        elif side == 'left':
            self.left_penalty = penalty

# =============================================================================

    def set_referee_data(self, cmd, stage, stage_time, team_name, goals, yellow_c, red_c):
        self.referee_data.cmd = cmd
        self.referee_data.stage = stage
        self.referee_data.stage_time = stage_time
        self.referee_data.names = team_name
        self.referee_data.goals = goals
        self.referee_data.yellow_cards = yellow_c
        self.referee_data.red_cards = red_c

# =============================================================================

    def draw_field(self, scaled_field: np.array):
        adj_border = self.scale_val(FIELD_BORDER)

        field_rect = gz.rectangle(scaled_field[0], scaled_field[1],
                                  stroke=WHITE_C,
                                  stroke_width=FIELD_LINE_PEN_SZ)
        field_rect = field_rect.translate(xy=adj_border + scaled_field/2)

        for line in self.field_lines(scaled_field, adj_border):
            field_line = gz.polyline(line, stroke=WHITE_C,
                                     stroke_width=FIELD_LINE_PEN_SZ)
            field_line.draw(self.surface)

        field_circle = gz.circle(self.scale_rad(self.center_circle_radius),
                                 xy=self.adjust_axis(np.array([0, 0]),
                                                     scaled_field),
                                 stroke=WHITE_C,
                                 stroke_width=FIELD_LINE_PEN_SZ)

        field_rect.draw(self.surface)
        field_circle.draw(self.surface)

# =============================================================================

    def draw_goal(self, scaled_field: np.array, side: str):
        goal_pos = (0, 0)
        adj_border = self.scale_val(FIELD_BORDER)
        scaled_goal = self.scale_val(self.goal_size)

        if side == 'left':
            goal_pos = (adj_border[0] - scaled_goal[0]/2,
                        adj_border[1]+scaled_field[1]/2)
        elif side == 'right':
            goal_pos = (adj_border[0] + scaled_field[0] + scaled_goal[0]/2,
                        adj_border[1]+scaled_field[1]/2)
        else:
            return

        field_rect = gz.rectangle(scaled_goal[0], scaled_goal[1],
                                  stroke=BLACK_C,
                                  stroke_width=FIELD_LINE_PEN_SZ)
        field_rect = field_rect.translate(xy=goal_pos)
        field_rect.draw(self.surface)


# =============================================================================

    def draw_ball(self, scaled_field: np.array):
        ball_p = self.scale(self.ball, scaled_field)

        if isinstance(scaled_field, np.ndarray) or isinstance(ball_p, np.ndarray) or \
                len(ball_p) + len(scaled_field) == 4:
            ball = gz.circle(self.scale_rad(BALL_RADIUS),
                             xy=ball_p, fill=ORANGE_C)
            ball.draw(self.surface)

            ball = gz.circle(self.scale_rad(BALL_RADIUS)*10,
                             xy=ball_p, stroke=ORANGE_C, stroke_width=2)
            ball.draw(self.surface)

# =============================================================================

    def draw_robots(self, scaled_field: np.array):
        for bot in self.blue_robots:
            if bot.pos.x < INF:
                b_pos = np.array([bot.pos.x, bot.pos.y])
                self.draw_bot(b_pos, bot.pos.orientation, BLUE_C, scaled_field,
                              bot.id)

        for bot in self.yellow_robots:
            if bot.pos.x < INF:
                b_pos = np.array([bot.pos.x, bot.pos.y])
                self.draw_bot(b_pos, bot.pos.orientation, YELLOW_C, scaled_field,
                              bot.id)

    def draw_bot(self, pos: np.array, orientation: float, color,
                 scaled_field: np.array, id: int):
        # Both arguments must be a numpy array and have a length of 2 each
        if not isinstance(scaled_field, np.ndarray) or \
                not isinstance(pos, np.ndarray) or \
                len(pos) + len(scaled_field) != 4:
            red_print('Invalid data received ', pos, scaled_field, type(pos),
                      type(scaled_field))
            return

        b_pos = self.scale(pos, scaled_field)
        id_pos = self.scale(pos - np.array([0.1*BOT_RADIUS, -0.2*BOT_RADIUS]),
                            scaled_field)

        bot_rad = self.scale_rad(BOT_RADIUS)
        bot_teta = self.convert_orientation(orientation)

        teta = acos(1.0 - 336.0/289.0)
        start_angle = bot_teta - teta / 2
        span_angle = 2*pi - teta

        bot_circle = gz.arc(r=bot_rad,
                            a1=-start_angle, a2=-start_angle + span_angle,
                            fill=color, xy=b_pos)

        bot_id_text = gz.text(f'{id}', fontfamily='Arial', fontsize=20,
                              fontweight='bold', fill=BLACK_C, xy=id_pos)

        bot_circle.draw(self.surface)
        bot_id_text.draw(self.surface)

    def convert_orientation(self, orientation: float) -> float:
        if abs(orientation) > 2*pi:
            factor = orientation/(2*pi) - trunc(orientation/(2*pi))
            orientation = factor * 2*pi

        if orientation < 0:
            orientation += 2*pi

        if orientation > 2*pi:
            orientation = orientation - 2*pi

        return orientation

# =============================================================================

    def draw_penalty(self, penalty: [], scaled_field: np.array):
        scaled_penalty = [self.scale(np.array(point), scaled_field)
                          for point in penalty]
        penalty_lines = gz.polyline(scaled_penalty, False,
                                    stroke=WHITE_C, stroke_width=FIELD_LINE_PEN_SZ)
        penalty_lines.draw(self.surface)

# =============================================================================

    def show_referee_data(self, scaled_field):
        adj_border = self.scale_val(FIELD_BORDER)

        blue_pos = (adj_border[0] + scaled_field[0]/4, adj_border[1]/2)
        yellow_pos = (adj_border[0] + 3/4*scaled_field[0], adj_border[1]/2)

        self.draw_team_name_score(BLUE_TEAM, blue_pos)
        self.draw_team_name_score(YELLOW_TEAM, yellow_pos)

        vs_text = gz.text('X', fontfamily='Arial', fontsize=35,
                          fontweight='bold', fill=BLACK_C,
                          xy=(scaled_field[0]/2 + adj_border[0], adj_border[1]/2))

        card_pos = np.array([adj_border[0], adj_border[1]/2])
        self.draw_team_cards(BLUE_TEAM, card_pos)

        card_pos[0] = adj_border[0] + \
            scaled_field[0] - 4*self.scale_val(CARD_SZ)[0]
        self.draw_team_cards(YELLOW_TEAM, card_pos)

        vs_text.draw(self.surface)
        self.draw_referee_command(scaled_field, adj_border)
        self.draw_referee_stage(scaled_field, adj_border)
        self.draw_referee_stage_time(scaled_field, adj_border)

# =============================================================================

    def draw_team_name_score(self, team: str, pos: np.array):
        color = YELLOW_C
        if team == BLUE_TEAM:
            color = BLUE_C
        score = f'{self.referee_data.names[team]} {self.referee_data.goals[team]}'
        team_text = gz.text(score, fontfamily='Arial', fontsize=25,
                            fontweight='bold', fill=color, xy=pos)
        team_text.draw(self.surface)

# =============================================================================

    def draw_team_cards(self, team: str, pos: np.array):
        self.draw_card(team, pos, self.referee_data.yellow_cards[team],
                       YELLOW_C)

        pos[0] = pos[0] + 3*self.scale_val(CARD_SZ)[0]
        self.draw_card(team, pos, self.referee_data.red_cards[team], RED_C)

    def draw_card(self, team: str, pos: np.array, card_count: int, color):
        card_sz = np.array(CARD_SZ)
        card_sz = self.scale_val(card_sz)
        text_color = YELLOW_C

        if team == BLUE_TEAM:
            text_color = BLUE_C

        card = gz.rectangle(card_sz[0], card_sz[1], fill=color, xy=pos)

        count = gz.text(f'{card_count}', fontfamily='Arial', fontsize=25,
                        fontweight='bold', fill=text_color,
                        xy=(pos[0] + card_sz[0]*1.1, pos[1]))

        card_indicator = gz.Group([card, count])
        card_indicator.draw(self.surface)

# =============================================================================

    def draw_referee_command(self, scaled_field: np.array, adj_border: np.array):
        ref_cmd_str = self.referee_data.cmd.replace('_', ' ')
        ref_cmd = gz.text(f'{ref_cmd_str}', fontfamily='Arial',
                          fontsize=20, fontweight='bold', fill=BLACK_C,
                          xy=(scaled_field[0]/4 + adj_border[0],
                              scaled_field[1] + 3/2*adj_border[1]))
        ref_cmd.draw(self.surface)

    def draw_referee_stage(self, scaled_field: np.array, adj_border: np.array):
        stage_str = self.referee_data.stage.replace('_', ' ')

        ref_cmd = gz.text(f'{stage_str}', fontfamily='Arial',
                          fontsize=16, fontweight='bold', fill=BLACK_C,
                          xy=(adj_border[0] + 3/4*scaled_field[0],
                              scaled_field[1] + 3/2*adj_border[1]))
        ref_cmd.draw(self.surface)

    def draw_referee_stage_time(self, scaled_field: np.array, adj_border: np.array):
        ref_cmd = gz.text(f'{self.referee_data.stage_time}', fontfamily='Arial',
                          fontsize=16, fontweight='bold', fill=BLACK_C,
                          xy=(adj_border[0] + scaled_field[0]/2,
                              scaled_field[1] + 3/2*adj_border[1]))
        ref_cmd.draw(self.surface)
# =============================================================================

    def update_robots(self, robots: [Robot], team: str):
        if team == YELLOW_TEAM:
            self.yellow_robots = robots
        elif team == BLUE_TEAM:
            self.blue_robots = robots

# =============================================================================

    def update_ball(self, pos: Position):
        self.ball = np.array([pos.x, pos.y])

# =============================================================================

    def scale(self, pos: np.array, field_sz=None) -> np.array:
        field_sz_border = self.field_size + \
            np.array([2*FIELD_BORDER, 2*FIELD_BORDER])
        scale_val = np.divide(np.array(SCREEN_SIZE), field_sz_border)

        if not isinstance(field_sz, np.ndarray):
            return np.multiply(pos, scale_val)

        return self.adjust_axis(np.multiply(pos, scale_val), field_sz)

    def scale_val(self, val) -> [float, float]:
        field_sz_border = self.field_size + \
            np.array([2*FIELD_BORDER, 2*FIELD_BORDER])
        scale_val = np.divide(np.array(SCREEN_SIZE), field_sz_border)

        return val * scale_val

    def scale_rad(self, val) -> float:
        field_sz_border = self.field_size + \
            np.array([2*FIELD_BORDER, 2*FIELD_BORDER])
        scale_val = np.divide(np.array(SCREEN_SIZE), field_sz_border)

        return val * sqrt(scale_val[0]**2 + scale_val[1]**2)

    def adjust_axis(self, pos: np.array, field_sz: np.array) -> np.array:
        border = self.scale_val(FIELD_BORDER)
        adj_p = np.add(pos, field_sz/2)
        adj_p[1] = field_sz[1] - adj_p[1]
        adj_p = np.add(adj_p, border)

        return adj_p

# =============================================================================

    def field_lines(self, field_sz: np.array, field_border: np.array):
        # Middle line
        l1 = np.array([[field_sz[0]/2 + field_border[0], field_border[1]],
                       [field_sz[0]/2 + field_border[0], field_sz[1] + field_border[1]]])

        # Horizontal half line
        l2 = np.array([[field_border[0], field_sz[1]/2 + field_border[1]],
                       [field_sz[0] + field_border[0], field_sz[1]/2 + field_border[1]]])

        return [l1.tolist(), l2.tolist()]

# =============================================================================
