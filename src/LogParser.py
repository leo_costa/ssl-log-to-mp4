import gzip
import struct
import datetime
import numpy as np
from os.path import isfile, splitext
from copy import deepcopy

import ssl_vision_detection_pb2 as detection
import ssl_vision_geometry_pb2 as geometry
import ssl_wrapper_pb2 as wrapper
import referee_pb2 as referee

from ssl_vision_geometry_pb2 import SSL_GeometryData
from aux.utils import red_print, blue_print, green_print, purple_print
from aux.RobotBall import BLUE_TEAM, YELLOW_TEAM

FILE_FORMAT_SIZE = 16  # 16 bytes = int64 + 2 x int32

MESSAGE_BLANK = 0
MESSAGE_UNKNOWN = 1
MESSAGE_SSL_VISION_2010 = 2
MESSAGE_SSL_REFBOX_2013 = 3
MESSAGE_SSL_VISION_2014 = 4
MESSAGE_SSL_VISION_TRACKER_2020 = 5
MESSAGE_SSL_INDEX_2021 = 6


class LogData(object):
    def __init__(self, detection={}, referee={}, geometry={}, timestamp=0,
                 end=False):
        self.detection = detection
        self.referee = referee
        self.geometry = geometry
        self.timestamp = timestamp
        self.end = end

    def update_data(self, log_data):
        self.detection = log_data.detection.copy()
        if len(log_data.referee) > 0:
            self.referee = log_data.referee.copy()
        if len(log_data.geometry) > 0:
            self.geometry = log_data.geometry.copy()
        self.timestamp = log_data.timestamp
        self.end = log_data.end

    def __deepcopy__(self, memo):  # memo is a dict of id's to copies
        id_self = id(self)        # memoization avoids unnecesary recursion
        _copy = memo.get(id_self)
        if _copy is None:
            _copy = type(self)(
                deepcopy(self.detection, memo),
                deepcopy(self.referee, memo),
                deepcopy(self.geometry, memo),
                deepcopy(self.timestamp, memo),
                deepcopy(self.end, memo))
            memo[id_self] = _copy
        return _copy


class LogParser(object):
    def __init__(self, log_filename: str, video_fps: int):
        self.log_file = None
        self.log_filename = log_filename
        self.total_time = 0
        self.real_fps = 0
        self.video_fps = video_fps
        self.frame_count = 0

        self.open_log_file()

        self.team_name = {YELLOW_TEAM: 'Unknown', BLUE_TEAM: 'Unknown'}
        self.team_scores = {YELLOW_TEAM: 0, BLUE_TEAM: 0}
        self.team_yellow_cards = {YELLOW_TEAM: 0, BLUE_TEAM: 0}
        self.team_red_cards = {YELLOW_TEAM: 0, BLUE_TEAM: 0}
        self.bytes_read = 0

        self.wrapper_frame = wrapper.SSL_WrapperPacket()
        self.referee_frame = referee.SSL_Referee()

        self.filtered_log_data = []

# =============================================================================

    def open_log_file(self):
        if isfile(self.log_filename):
            try:
                if splitext(self.log_filename)[1] == '.gz':
                    self.log_file = gzip.open(self.log_filename, 'rb')
                else:
                    self.log_file = open(self.log_filename, 'rb')

            except Exception as expt:
                red_print(f'Failed to open log file. Exception = {expt}')
                quit(1)
        else:
            red_print(
                f'[LOG ERROR] Invalid path: {self.log_filename} | {isfile(self.log_filename)}')

        if not self.check_log_file():
            quit(1)

# =============================================================================

    def get_log_header(self):
        header = self.log_file.read(12)  # length of 'SSL_LOG_FILE'
        return header

# =============================================================================

    def get_log_version(self):
        version = int.from_bytes(self.log_file.read(4), byteorder="big")
        return version

# =============================================================================

    def get_decoded_msg(self, bin_line):
        # >qii -> format for int64 + int32 + int32 in big-endian
        data = struct.unpack('>qii', bin_line)
        timestamp = datetime.timedelta(seconds=data[0]/1e9).total_seconds()
        msg_type = data[1]
        msg_sz = data[2]
        return timestamp, msg_type, msg_sz

# =============================================================================

    def get_msg_format(self):
        format = self.log_file.read(FILE_FORMAT_SIZE)
        self.bytes_read += 16
        return format

# =============================================================================

    def check_log_file(self) -> bool:
        fileOK = False
        if self.log_file == None:
            red_print('Log file == None')
        else:
            header = self.get_log_header()
            version = self.get_log_version()

            if (header == b'SSL_LOG_FILE'):
                green_print(f'[LOG INFO] Header {header}')
                fileOK = True
            else:
                red_print(
                    f'[LOG ERROR] Wrong file header. Got header = {header}')
                fileOK = False

            if (version == 1):
                green_print(f'[LOG INFO] Version {version}')
                fileOK = True & fileOK
            else:
                red_print(
                    f'[LOG ERROR] Wrong file version. Got version = {version}')
                fileOK = False & fileOK

        return fileOK

# =============================================================================

    def read_log_file(self) -> (int, float):
        log_data = LogData()
        self.frame_count = all_frames = timestamp = self.total_time = 0
        last_cmd = 'HALT'
        skipping_halt_stop = False

        while not log_data.end:
            log_data.update_data(deepcopy(self.get_log_data()))

            if len(log_data.referee) > 0:
                last_cmd = log_data.referee['Command']

            if len(log_data.detection) > 0:
                all_frames += 1
                if last_cmd not in ['STOP', 'HALT']:
                    blue_print(
                        "[LOG INFO] Reading log file... | Frames = {}".format(round(self.frame_count)), '\r')
                    skipping_halt_stop = False
                    self.frame_count += 1
                    self.total_time += log_data.timestamp - timestamp
                    timestamp = log_data.timestamp
                    self.filtered_log_data.append(deepcopy(log_data))
                else:
                    timestamp = log_data.timestamp
                    if not skipping_halt_stop:
                        skipping_halt_stop = True
                        red_print("\n[LOG INFO] Skipping Halt or Stop")

        self.real_fps = round(self.frame_count / self.total_time)
        self.total_time = round(self.total_time)
        blue_print(
            f'[LOG INFO] Total = {all_frames} frames | Non STOP/HALT = {self.frame_count} frames | Total time = {self.total_time} seconds')
        return self.frame_count, self.total_time

# =============================================================================

    def process_vision_log(self, bin_data) -> bool:
        if bin_data is None:
            return False
        data_ok = False

        if len(bin_data) > 0:
            try:
                self.wrapper_frame.ParseFromString(bin_data)
                data_ok = True

            except Exception as except_type:
                red_print(except_type, type(except_type))

        return data_ok

# =============================================================================

    def process_referee_log(self, bin_data) -> bool:
        if bin_data is None:
            return False
        data_ok = False

        if len(bin_data) > 0:
            try:
                self.referee_frame.ParseFromString(bin_data)
                data_ok = True
            except Exception as except_type:
                red_print(except_type)

        return data_ok

# =============================================================================

    def detection_frame_to_dict(self, detection_frame: detection.SSL_DetectionFrame) -> dict:
        frame_dict = dict()

        if not isinstance(detection_frame, detection.SSL_DetectionFrame):
            return frame_dict

        ball_pos = [[round(ball.x), round(ball.y), 0]
                    for ball in detection_frame.balls]

        # Use the mean in case there is more than one ball
        if len(ball_pos) > 0:
            mean_pos = np.mean(np.array(ball_pos), axis=0)
            ball_pos = mean_pos.tolist()

        ball = {'pos': ball_pos}

        blue_robots = [{'obj': {'pos': [round(robot.x), round(robot.y), robot.orientation]},
                        'id': {'number': robot.robot_id, 'color': BLUE_TEAM}}
                       for robot in detection_frame.robots_blue]

        yellow_robots = [{'obj': {'pos': [round(robot.x), round(robot.y), robot.orientation]},
                          'id': {'number': robot.robot_id, 'color': YELLOW_TEAM}}
                         for robot in detection_frame.robots_yellow]

        frame_dict['ball'] = ball
        frame_dict['bots'] = blue_robots
        frame_dict['bots'].extend(yellow_robots)

        return frame_dict

# =============================================================================

    def geometry_frame_to_dict(self, geometry_frame: geometry.SSL_GeometryData) -> dict():
        frame_dict = dict()

        if not isinstance(geometry_frame, geometry.SSL_GeometryData):
            return frame_dict

        if geometry_frame.field.field_length != 0:
            frame_dict['field_size'] = [geometry_frame.field.field_length,
                                        geometry_frame.field.field_width]
            frame_dict['goal_size'] = [geometry_frame.field.goal_depth,
                                       geometry_frame.field.goal_width]

            frame_dict['right_penalty'] = [[0, 0], [0, 0], [0, 0], [0, 0]]
            frame_dict['left_penalty'] = [[0, 0], [0, 0], [0, 0], [0, 0]]
            frame_dict['center_circle'] = 0

            for arc in geometry_frame.field.field_arcs:
                if arc.name == 'CenterCircle':
                    frame_dict['center_circle'] = arc.radius

            for line in geometry_frame.field.field_lines:
                if line.name == 'LeftFieldLeftPenaltyStretch':
                    frame_dict['left_penalty'][0] = [line.p1.x, line.p1.y]
                    frame_dict['left_penalty'][1] = [line.p2.x, line.p2.y]
                elif line.name == 'LeftFieldRightPenaltyStretch':
                    frame_dict['left_penalty'][2] = [line.p2.x, line.p2.y]
                    frame_dict['left_penalty'][3] = [line.p1.x, line.p1.y]

                if line.name == 'RightFieldLeftPenaltyStretch':
                    frame_dict['right_penalty'][0] = [line.p1.x, line.p1.y]
                    frame_dict['right_penalty'][1] = [line.p2.x, line.p2.y]
                elif line.name == 'RightFieldRightPenaltyStretch':
                    frame_dict['right_penalty'][2] = [line.p2.x, line.p2.y]
                    frame_dict['right_penalty'][3] = [line.p1.x, line.p1.y]
        return frame_dict

# =============================================================================

    def get_vision_log_data(self, log_data) -> (dict, dict):
        vis_ok = self.process_vision_log(log_data)

        if vis_ok:
            return (self.detection_frame_to_dict(self.wrapper_frame.detection),
                    self.geometry_frame_to_dict(self.wrapper_frame.geometry))
        return ({}, {})

# =============================================================================

    def get_referee_log_data(self, log_data) -> dict:
        ref_ok = self.process_referee_log(log_data)
        ret_val = {}

        if ref_ok:
            self.update_team_data()
            stage_time = str(datetime.timedelta(
                microseconds=self.referee_frame.stage_time_left))

            ret_val = {'Command': self.referee_frame.Command.Name(self.referee_frame.command),
                       'Stage': self.referee_frame.Stage.Name(self.referee_frame.stage),
                       'StageTime': stage_time,
                       'TeamNames': self.team_name,
                       'TeamScores': self.team_scores,
                       'TeamYellowC': self.team_yellow_cards,
                       'TeamRedC': self.team_red_cards}

        return ret_val

# =============================================================================

    def update_team_data(self):
        # try/except just to make sure it doesn't crash if the team name doesn't
        # exist for some reason
        try:
            name = self.referee_frame.yellow.name
            self.team_name[YELLOW_TEAM] = name
            self.team_scores[YELLOW_TEAM] = self.referee_frame.yellow.score
            self.team_yellow_cards[YELLOW_TEAM] = self.referee_frame.yellow.yellow_cards
            self.team_red_cards[YELLOW_TEAM] = self.referee_frame.yellow.red_cards
        except Exception:
            pass

        try:
            name = self.referee_frame.blue.name
            self.team_name[BLUE_TEAM] = name
            self.team_scores[BLUE_TEAM] = self.referee_frame.blue.score
            self.team_yellow_cards[BLUE_TEAM] = self.referee_frame.blue.yellow_cards
            self.team_red_cards[BLUE_TEAM] = self.referee_frame.blue.red_cards
        except Exception:
            pass

# =============================================================================

    def get_log_data(self) -> LogData:
        log_data = LogData()
        if self.log_file.closed:
            red_print('[LOG ERROR] Log file is closed!')
            return log_data

        format = self.get_msg_format()

        msg_sz = 0
        try:
            log_data.timestamp, msg_type, msg_sz = self.get_decoded_msg(format)
        except:
            pass

        if msg_sz > 0:
            msg_data = self.log_file.read(msg_sz)
            self.bytes_read += msg_sz

            if msg_type == MESSAGE_SSL_REFBOX_2013:
                log_data.referee = self.get_referee_log_data(msg_data)

            elif msg_type == MESSAGE_SSL_VISION_2014:
                log_data.detection, log_data.geometry = \
                    self.get_vision_log_data(msg_data)
        else:
            log_data.end = True
            self.log_file.close()

        return log_data

# =============================================================================

    def get_video_frame_data(self, t) -> LogData:
        data_index = round(t*self.real_fps)

        if data_index >= self.frame_count:
            data_index = self.frame_count - 1
        return self.filtered_log_data[data_index]

# =============================================================================

    def get_data_by_frame(self, i) -> LogData:
        data_index = i

        if data_index >= self.frame_count:
            data_index = self.frame_count - 1
        return self.filtered_log_data[data_index]
